/*
 * grunt-copy-mate
 * https://bitbucket.org/jimdoyle82/grunt-copy-mate/overview
 *
 * Copyright (c) 2013 James Doyle
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  grunt.initConfig({
    clean: [ "test/result" ]
    
    ,copy_mate: {

      default_opts: {
        options: {
          type: "single"
          ,destName: "test.js"
        }
        ,src: 'test/test3.js'
        ,destDir: "test/result/"
      }

      ,batch_opts: {
        options: {
          type: "batch"
        }
        ,src: ['test/test1.js', 'test/test2.js', 'test/test3.js']
        ,destDir: "test/result"
      }

      ,recursive_opts: {
        options: {
          type: "recursive"
          // ,singleLevel: true
          // ,specificFileName: "test1.js"
          ,excludedDirs: ["inner-test2"]
        }
        ,src: "test"
        ,destDir: "test/result"
      }

    }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // load npm deps
  grunt.loadNpmTasks('grunt-contrib-clean');

  // By default, lint and run all tests.
  grunt.registerTask('default', [ 'clean', 'copy_mate:recursive_opts']);


  /**
   * Non-config examples
   */

  grunt.registerTask("non-config-example-single", function() {

    grunt.copy_mate.singleCopy("test/test1.js", "test/result/non-config-example/single/");
  });


  grunt.registerTask("non-config-example-batch", function() {

    grunt.copy_mate.batchCopy([ 'test/test1.js', 'test/test2.js', 'test/test3.js' ], "test/result/non-config-example/batch/" );
  });


  grunt.registerTask("non-config-example-recursive", function() {

    grunt.copy_mate.recursiveCopy("test", "test/result/non-config-example/recursive/", false, "test1.js", "inner-test2" );
  });
};
