# grunt-copy-mate

> DEPRECATED: Very old and unmaintained module. Don't recommend using this anymore.

> Utilities for copying files and folders.

## Getting Started
This plugin requires Grunt `0.4.x`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-copy-mate --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-copy-mate');
```

## The "copy_mate" task

### Overview
In your project's Gruntfile, add a section named `copy_mate` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  copy_mate: {
    options: {
      type: "single"
    },
    src: "test",
    destDir: "test/result"
  },
});
```

### Options

#### options.type
Type: `String`
Default value: `'single'`

A string value that is used choose a method of copying files. Can be 'single', 'batch' or 'recursive'.

- 'single' will copy a single file from the given 'src' file path to the 'destDir' directory path, both of which must be string values. An option may be passed 'optionalDestName'. More detail below.

- 'batch' will copy an array of files from the given 'src' property to the 'destDir' directory path. 'src' must be an array and 'destDir' must be a string. No additional options are supported with this 'type'.

- 'recursive' will recursively copy all files from the given 'src' file path to the 'destDir' directory path, both of which must be string values. It accespts 3 additional options. 'singleLevel' (boolean), 'specificFileName' (string) and 'excludedDirs' (array or string). More detail on these options is below.

#### options.optionalDestName
Type: `String`
Default value: `undefined`

Only supported with option.type = 'single'. Used to specify a custom file name, once copied to the 'destDir'.

#### options.singleLevel
Type: `Boolean`
Default value: `false`

Only supported with option.type = 'recursive'. Tells task to only copy from the 1st directory, ignoring all subdirectories.

#### options.specificFileName
Type: `String`
Default value: `undefined`

Only supported with option.type = 'recursive'. Tells task to only copy files with this file name from all directories.

#### options.excludedDirs
Type: `Array or String`
Default value: `undefined`

Only supported with option.type = 'recursive'. Tells task to omit specified directories (and their child directories). Can be a single string or an array of strings.


### Usage Examples

#### Default Options
In this example, the default options are used to copy 'src' file path to 'destDir' directory path.

```js
grunt.initConfig({
  copy_mate: {
    src: 'test/test3.js'
    ,destDir: "test/result/"
  },
});
```

#### Custom Options
Below are examples of 'single', 'batch' and 'recursive' tasks.

```js
grunt.initConfig({
  copy_mate: {

    single_opts: {
      options: {
        type: "single"
        ,destName: "test.js"
      }
      ,src: 'test/test3.js'
      ,destDir: "test/result/"
    }

    ,batch_opts: {
      options: {
        type: "batch"
      }
      ,src: ['test/test1.js', 'test/test2.js', 'test/test3.js']
      ,destDir: "test/result"
    }

    ,recursive_opts: {
      options: {
        type: "recursive"
        // ,singleLevel: true
        // ,specificFileName: "test1.js"
        ,excludedDirs: ["inner-test2"]
      }
      ,src: "test"
      ,destDir: "test/result"
    }
  }
});
```


#### Non-config usage
Sometimes you may be writing custom tasks in your Gruntfile.js and you want to use the copy features of this task, without actually running it. 
'recursiveCopy' will also return an array of file paths copied. 
Examples of how to do this below:

```js

// single file
grunt.registerTask("non-config-example-single", function() {

  grunt.copy_mate.singleCopy("test/test1.js", "test/result/non-config-example/single/");
});


// batch files
grunt.registerTask("non-config-example-batch", function() {

  grunt.copy_mate.batchCopy([ 'test/test1.js', 'test/test2.js', 'test/test3.js' ], "test/result/non-config-example/batch/" );
});


// recursive
grunt.registerTask("non-config-example-recursive", function() {

  grunt.copy_mate.recursiveCopy("test", "test/result/non-config-example/recursive/", false, "test1.js", "inner-test2" );
});
```



## Release History
0.1.2 - Added  'createSmallNames' argument for 'recursiveCopy'.
0.1.1 - Added 'non-config' functions for use in regular tasks, along with some examples.
0.1.0 - Initial port over from grunt.jimd.utils, with some improvements made to 'recursive'.
