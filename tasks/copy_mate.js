/*
 * grunt-copy-mate
 * https://bitbucket.org/jimdoyle82/grunt-copy-mate/overview
 *
 * Copyright (c) 2013 James Doyle
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  var RECURSIVE = "recursive"
      ,BATCH = "batch"
      ,SINGLE = "single";

  var type = function ( test ) {
    return ( {} ).toString.call( test ).match( /\s([a-zA-Z]+)/ )[ 1 ].toLowerCase();
  }
  ,singleCopy = function( src, destDir, optionalDestName ) {
    if( optionalDestName && type(optionalDestName) !== 'string' )   grunt.log.warn("Warning: option 'optionalDestName' won't work unless it is a string.");

    var destName = src.split('/').pop();

    grunt.file.copy( src, destDir + ( optionalDestName ? optionalDestName : destName ) );
  }
  ,batchCopy = function( srcArr, destDir ) {

    var i = 0, len = srcArr.length, src, destName;
    for( ; i<len; i++ ) {
      src     = srcArr[i];
      destName  = src.split('/').pop();

      grunt.file.copy( src, destDir + destName );
    }
  }
  ,recursiveCopy = function( src, destDir, singleLevel, specificFileName, excludedDirs, createSmallNames ) {
    
    if( singleLevel      && type(singleLevel)      !== 'boolean' )  grunt.log.warn("Warning: option 'singleLevel' won't work unless it is a boolean.");
    if( specificFileName && type(specificFileName) !== 'string' )   grunt.log.warn("Warning: option 'specificFileName' won't work unless it is a string.");
    
    var arrayOrString = excludedDirs     && type(excludedDirs)     === 'array' || 
                        excludedDirs     && type(excludedDirs)     === 'string';
    if( excludedDirs && !arrayOrString ) grunt.log.warn("Warning: option 'excludedDirs' won't work unless it is a array or a string.");

    var specificExtension;
    if( specificFileName )
      if( specificFileName.charAt(0) === "*" )
        specificExtension = specificFileName.split("*")[1]


    var curDir
       ,doExclude
       ,curDirIncr = -1
       ,lastCurDir = ''
       ,rtnMap = [];
    grunt.file.recurse( src, function( abspath, rootdir, subdir, filename ) {

      if( filename != "_DS_Store" && filename != ".DS_Store" && filename != "Thumbs.db" && filename != "Thumbs.db.doc" && filename != "desktop.ini" ) {

        curDir = '';
        if( subdir != undefined )
          curDir = subdir + '/';

        // determine if "excludedDirs" was a string or an array of strings 
        doExclude = false;
        if( excludedDirs ) {

          var splitArr = curDir.split("/");

          if( (type( excludedDirs ) === 'string') && splitArr.indexOf( excludedDirs ) !== -1 ) {

            // If single string
            doExclude = true;
          } else {
            // if an array of strings

            var exclDir, curDirSplit;
            for( var i=0; i<excludedDirs.length; i++ ) {
              exclDir = excludedDirs[i];

              if( type(exclDir) === "string" ) {

                for( var j=0; j<splitArr.length; j++ ) {
                  curDirSplit = splitArr[j];

                  if( curDirSplit === exclDir ) {
                    doExclude = true;
                    break;
                  }
                  
                }

                // if 2nd loop stopped, stop this one too.
                if( doExclude === true ) break;
              } else {
                grunt.log.warn( "Warning: excludedDirs item " + exclDir + " was skipped, because it was not a string value." );
              }

            }

          } // END if( (type( excludedDirs ) === 'string')

        } // END if( excludedDirs )

        
        if( doExclude === false ) {

          if( !singleLevel || singleLevel === true && curDir === '' ) {

            if( !specificFileName || // if no specific file defined
              specificFileName === filename || // if a specific is file
              specificExtension && specificExtension.length === 0 || // if * (all) requested
              specificExtension && specificExtension === filename.slice( (specificExtension.length * -1) ) // if a specific extension defined
            ) {
              if( lastCurDir !== curDir ) curDirIncr++;

              var thisCurDir = ( createSmallNames === true ? "tmp_" + curDirIncr + "/" : curDir );
              
              grunt.file.copy( abspath, destDir + thisCurDir + filename );

              rtnMap.push({
                id: curDirIncr
                ,createSmallNames: !!(createSmallNames)
                ,curDir: thisCurDir
                ,destDir: destDir
                ,originalDir: curDir
                ,filename: filename
              });

              lastCurDir = curDir;
            }
          }
        }
      } // end if file types
    });
  
    return rtnMap;
  }
  ,nonRescursiveWarning = function( opts, optType ) {

    if( opts.singleLevel )       grunt.log.warn( "Warning: option 'singleLevel' not supported with 'type = "     +optType+"'." );
    if( opts.specificFileName )  grunt.log.warn( "Warning: option 'specificFileName' not supported with 'type = "+optType+"'." );
    if( opts.excludedDirs )      grunt.log.warn( "Warning: option 'excludedDirs' not supported with 'type = "    +optType+"'." );
  }
  ,nonSingleWarning = function( opts, optType ) {
    if( opts.destName ) grunt.log.warn( "Warning: option 'destName' not supported with 'type = "+optType+"'." );
  }
  ,srcCheckPlusWarning = function( data, optType, shouldBeString ) {
    var srcType = shouldBeString ? "string" : "array";

    if( type( data.src ) != srcType ) {
      grunt.log.error( "Error: grunt-copy-mate. When used with 'type = "+optType+"', the 'src' must be a "+srcType+"." );
      return false;
    }

    return true;
  }
  ,destDirCheckPlusWarning = function( data, optType ) {
    var destDirType = "string";

    if( type( data.destDir ) != destDirType ) {
      grunt.log.error( "Error: grunt-copy-mate. When used with 'type = "+optType+"', the 'src' must be a "+destDirType+"." );
      return false;
    }

    return true;
  };


  /**
   * If you want to run these functions from a custom task, rather than from the config, use like this:
   * grunt.copy_mate.singleCopy("test/test1.js", "test/result/non-config-example/");
   */
  grunt.copy_mate = grunt.copy_mate || {};
  grunt.copy_mate.singleCopy = singleCopy;
  grunt.copy_mate.recursiveCopy = recursiveCopy;
  grunt.copy_mate.batchCopy = batchCopy;

  
  grunt.registerMultiTask('copy_mate', 'Utilities for copying files and folders.', function() {

    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
          type: SINGLE
          ,createSmallNames: false
        })
        ,data = this.data;
    
    // check destDir is a string
    if(  !destDirCheckPlusWarning( data, options.type )  ) return;

    var lastDestChar = data.destDir.charAt( data.destDir.length-1 );

    // Make sure the destDir always ends in a '/'
    if( lastDestChar != '/' ) data.destDir += '/';

    switch( options.type ) {


      case SINGLE:
        if(  !srcCheckPlusWarning( data, SINGLE, true )  ) return;
        nonRescursiveWarning( options, SINGLE );
        singleCopy( data.src, data.destDir, options.destName );
        break;


      case BATCH:
        if(  !srcCheckPlusWarning( data, BATCH, false )  ) return;
        nonSingleWarning( options, BATCH );
        nonRescursiveWarning( options, BATCH );
        batchCopy( data.src, data.destDir );
        break;


      case RECURSIVE:
        if(  !srcCheckPlusWarning( data, RECURSIVE, true )  ) return;
        nonSingleWarning( options, RECURSIVE )
        recursiveCopy( data.src, data.destDir, options.singleLevel, options.specificFileName, options.excludedDirs, options.createSmallNames );
        break;

    }
  });
  

};
